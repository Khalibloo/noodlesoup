# README #

The Khalibloo Noodlesoup is a Blender addon that resulted from my experiments with visual (node) scripting in Blender. It works by generating a python script from the nodes and then the script can be inspected (and edited if necessary) in blender's text editor before running.

### Why haven't I heard of this before? ###
Visual scripting is no small task. And I'm unable to provide continued support for this at the moment, so I'm zipping my lips :D

### Installation ###
Install like any other Blender addon. Check the Blender manual if you're unsure. You'll find the addon in your node editor.