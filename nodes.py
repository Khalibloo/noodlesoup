import bpy
from bpy.types import NodeTree, Node, NodeSocket

# Utils
const_list = [
#Motion tracking
     ('CAMERA_SOLVER', 'Camera Solver', '', 'CONSTRAINT_DATA', 0),
     ('FOLLOW_TRACK', 'Follow Track', '', 'CONSTRAINT_DATA', 1),
     ('OBJECT_SOLVER', 'Object Solver', '', 'CONSTRAINT_DATA', 2),
     #Transform
     ('COPY_LOCATION', 'Copy Location', '', 'CONSTRAINT_DATA', 3),
     ('COPY_ROTATION', 'Copy Rotation', '', 'CONSTRAINT_DATA', 4),
     ('COPY_SCALE', 'Copy Scale', '', 'CONSTRAINT_DATA', 5),
     ('COPY_TRANSFORMS', 'Copy Transforms', '', 'CONSTRAINT_DATA', 6),
     ('LIMIT_DISTANCE', 'Limit Distance', '', 'CONSTRAINT_DATA', 7),
     ('LIMIT_LOCATION', 'Limit Location', '', 'CONSTRAINT_DATA', 8),
     ('LIMIT_ROTATION', 'Limit Rotation', '', 'CONSTRAINT_DATA', 9),
     ('LIMIT_SCALE', 'Limit Scale', '', 'CONSTRAINT_DATA', 10),
     ('MAINTAIN_VOLUME', 'Maintain Volume', '', 'CONSTRAINT_DATA', 11),
     ('TRANSFORM', 'Transformation', '', 'CONSTRAINT_DATA', 12),
     #Tracking
     ('CLAMP_TO', 'Clamp To', '', 'CONSTRAINT_DATA', 13),
     ('DAMPED_TRACK', 'Damped Track', '', 'CONSTRAINT_DATA', 14),
     ('IK', 'Inverse Kinematics', '', 'CONSTRAINT_DATA', 15),
     ('LOCKED_TRACK', 'Locked Track', '', 'CONSTRAINT_DATA', 16),
     ('SPLINE_IK', 'Spline IK', '', 'CONSTRAINT_DATA', 17),
     ('STRETCH_TO', 'Stretch To', '', 'CONSTRAINT_DATA', 18),
     ('TRACK_TO', 'Track To', '', 'CONSTRAINT_DATA', 19),
     #Relationship
     ('ACTION', 'Action', '', 'CONSTRAINT_DATA', 20),
     ('CHILD_OF', 'Child Of', '', 'CONSTRAINT_DATA', 21),
     ('FLOOR', 'Floor', '', 'CONSTRAINT_DATA', 22),
     ('FOLLOW_PATH', 'Follow Path', '', 'CONSTRAINT_DATA', 23),
     ('PIVOT', 'Pivot', '', 'CONSTRAINT_DATA', 24),
     ('RIGID_BODY_JOINT', 'Rigid Body Joint', '', 'CONSTRAINT_DATA', 25),
     ('SHRINKWRAP', 'Shrinkwrap', '', 'CONSTRAINT_DATA', 26)
    ]
mod_list = [
     #Modify
     ('MESH_CACHE', 'Mesh Cache', '', 'MOD_MESHDEFORM', 0),
     ('UV_PROJECT', 'UV Project', '', 'MOD_UVPROJECT', 1),
     ('UV_WARP', 'UV Warp', '', 'MOD_UVPROJECT', 2),
     ('VERTEX_WEIGHT_EDIT', 'Vertex Weight Edit', '', 'MOD_VERTEX_WEIGHT', 3),
     ('VERTEX_WEIGHT_MIX', 'Vertex Weight Mix', '', 'MOD_VERTEX_WEIGHT', 4),
     ('VERTEX_WEIGHT_PROXIMITY', 'Vertex Weight Proximity', '', 'MOD_VERTEX_WEIGHT', 5),
     #Generate
     ('ARRAY', 'Array', '', 'MOD_ARRAY', 6),
     ('BEVEL', 'Bevel', '', 'MOD_BEVEL', 7),
     ('BOOLEAN', 'Boolean', '', 'MOD_BOOLEAN', 8),
     ('BUILD', 'Build', '', 'MOD_BUILD', 9),
     ('DECIMATE', 'Decimate', '', 'MOD_DECIM', 10),
     ('EDGE_SPLIT', 'Edge Split', '', 'MOD_EDGESPLIT', 11),
     ('MASK', 'Mask', '', 'MOD_MASK', 12),
     ('MIRROR', 'Mirror', '', 'MOD_MIRROR', 13),
     ('MULTIRES', 'Multiresolution', '', 'MOD_MULTIRES', 14),
     ('REMESH', 'Remesh', '', 'MOD_REMESH', 15),
     ('SCREW', 'Screw', '', 'MOD_SCREW', 16),
     ('SKIN', 'Skin', '', 'MOD_SKIN', 17),
     ('SOLIDIFY', 'Solidify', '', 'MOD_SOLIDIFY', 18),
     ('SUBSURF', 'Subsurface Division', '', 'MOD_SUBSURF', 19),
     ('TRIANGULATE', 'Triangulate', '', 'MOD_TRIANGULATE', 20),
     ('WIREFRAME', 'Wireframe', '', 'MOD_WIREFRAME', 21),
     #Deform
     ('ARMATURE', 'Armature', '', 'MOD_ARMATURE', 22),
     ('CAST', 'Cast', '', 'MOD_CAST', 23),
     ('CURVE', 'Curve', '', 'MOD_CURVE', 24),
     ('DISPLACE', 'Displace', '', 'MOD_DISPLACE', 25),
     ('HOOK', 'Hook', '', 'HOOK', 26),
     ('LAPLACIANDEFORM', 'Laplacian Deform', '', 'MOD_MESHDEFORM', 27),
     ('LAPLACIANSMOOTH', 'Laplacian Smooth', '', 'MOD_SMOOTH', 28),
     ('LATTICE', 'Lattice', '', 'MOD_LATTICE', 29),
     ('MESH_DEFORM', 'Mesh Deform', '', 'MOD_MESHDEFORM', 30),
     ('SHRINKWRAP', 'Shrinkwrap', '', 'MOD_SHRINKWRAP', 31),
     ('SIMPLE_DEFORM', 'Simple Deform', '', 'MOD_SIMPLEDEFORM', 32),
     ('SMOOTH', 'Smooth', '', 'MOD_SMOOTH', 33),
     ('WARP', 'Warp', '', 'MOD_WARP', 34),
     ('WAVE', 'Wave', '', 'MOD_WAVE', 35),
     #Simulate
     ('CLOTH', 'Cloth', '', 'MOD_CLOTH', 36),
     ('COLLISION', 'Collision', '', 'MOD_PHYSICS', 37),
     ('DYNAMIC_PAINT', 'Dynamic Paint', '', 'MOD_DYNAMICPAINT', 38),
     ('EXPLODE', 'Explode', '', 'MOD_EXPLODE', 39),
     ('FLUID_SIMULATION', 'Fluid Simulation', '', 'MOD_FLUIDSIM', 40),
     ('OCEAN', 'Ocean', '', 'MOD_OCEAN', 41),
     ('PARTICLE_INSTANCE', 'Particle Instance', '', 'MOD_PARTICLES', 42),
     ('PARTICLE_SYSTEM', 'Particle System', '', 'MOD_PARTICLES', 43),
     ('SMOKE', 'Smoke', '', 'MOD_SMOKE', 44),
     ('SOFT_BODY', 'Soft Body', '', 'MOD_SOFT', 45)
     ]

datatype_list = [
        ('ACTIONS', 'Actions', ''), 
        ('BRUSHES', 'Brushes', ''), 
        ('GROUPS', 'Groups', ''), 
        ('IMAGES', 'Images', ''), 
        ('LINESTYLES', 'Freestyle Linestyles', ''),
        ('MATERIALS', 'Materials', ''),
        ('MOVIECLIPS', 'Movie Clips', ''),
        ('OBJECTS', 'Objects', ''),
        ('SCENES', 'Scenes', ''),
        ('SOUNDS', 'Sounds', ''),
        ('TEXTS', 'Texts', ''),
        ('TEXTURES', 'Textures', ''),
        ('WORLDS', 'Worlds', '')
    ]

template_import_bpy = "import bpy\n"
template_import_random = "import random\n"
use_random = False
template_value_init = "value = None\n"
template_tab = "    "
template_indent = "{"
template_unindent = "}"
template_linebreak = "\n"
template_random_int = "random.randint(, )\n" #ins at -4 and -2
template_random_float = "random.randint(,  - 1) + random.random()\n" #ins at 15 and 17
template_random_bool = "random.choice([True, False])"


def insertString(string, text, position):
    '''Insert string into text at position'''
    return text[:position] + string + text[position:]

def insertStrings(strings, text, positions):
    '''Insert strings into text at positions'''
    cummulative = 0
    result = []
    for i in range(0, len(positions)):
        result.append(text[cummulative:positions[i]])
        result.append(strings[i])
        cummulative = positions[i]
    result.append(text[cummulative:])
    return "".join(result)

def indentLines(string, indent):
    tab = "{"
    result = []
    for line in string.splitlines(keepends=True):
        line = (indent * tab) + line
        result.append(line)
    return "".join(result)

# Derived from the NodeTree base type, similar to Menu, Operator, Panel, etc.
class KhaliblooNoodleSoup(NodeTree):
    # Description string
    '''Visual scripting'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'KhaliblooNoodleSoup'
    # Label for nice name display
    bl_label = 'Khalibloo Noodle Soup'
    # Icon identifier
    bl_icon = 'NODETREE'


# Custom socket type
class VariableSocket(NodeSocket):
    # Description string
    '''Variable Socket'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'VariableSocket'
    # Label for nice name display
    bl_label = 'Variable'

    datatype = bpy.props.EnumProperty(items=(
                                ('INT', 'Integer', ''),
                                ('FLOAT', 'Float', ''),
                                ('STRING', 'String', ''),
                                ('BOOL', 'Boolean', ''),
                                ('COLOR', 'Color', ''),
                                ('VECTOR', 'Vector', ''),
                                ('DATA', 'Data', '')
                                ),
                                name = "Type",
                                description = "Type of variable",
                                default = 'FLOAT')
    int = bpy.props.IntProperty()
    min_random_int = bpy.props.IntProperty()
    max_random_int = bpy.props.IntProperty()
    float = bpy.props.FloatProperty()
    min_random_float = bpy.props.FloatProperty()
    max_random_float = bpy.props.FloatProperty()
    string = bpy.props.StringProperty()
    bool = bpy.props.BoolProperty()
    color = bpy.props.FloatVectorProperty(name = "Color",
                                        description = "Color value",
                                        default = (1.0, 1.0, 1.0, 1.0),
                                        min = 0.0,
                                        max = 1.0,
                                        subtype = 'COLOR',
                                        size = 4)
    vector2 = bpy.props.FloatVectorProperty(name = "Color",
                                        description = "Color value",
                                        size = 2)
    vector3 = bpy.props.FloatVectorProperty(name = "Color",
                                        description = "Color value",
                                        size = 3)
    vector4 = bpy.props.FloatVectorProperty(name = "Color",
                                        description = "Color value",
                                        size = 4)
    vector_size = bpy.props.IntProperty()
    result = bpy.props.StringProperty()
    data = bpy.props.StringProperty()
    
    def getValue(self):
        return self.result
    
    def setValue(self, value, datatype, use_random=False, min_random_int=0, max_random_int=10, vector_size=3):
        self.datatype = datatype
        if (datatype == 'INT'):
            self.int = value
            self.use_random = use_random
            if (use_random):
                self.min_random_int = min_random_int
                self.max_random_int = max_random_int
                self.result = insertStrings([str(min_random_int), str(max_random_int)], template_random_int, [-4, -2])
            else:
                self.result = str(value)
        elif (datatype == 'FLOAT'):
            self.float = value
            self.use_random = use_random
            if (use_random):
                self.min_random_int = min_random_int
                self.max_random_int = max_random_int
                self.result = insertStrings([str(min_random_int), str(max_random_int)], template_random_float, [15, 17])
            else:
                self.result = str(value)
        elif (datatype == 'STRING'):
            self.string = value
            self.result = "\"" + value + "\""
        elif (datatype == 'BOOL'):
            self.bool = value
            if (use_random):
                self.result = template_random_bool
            else:
                self.result = str(value)
        elif (datatype == 'COLOR'):
            self.color = value
            self.result = "(" + str(value[0]) + ", " + str(value[1]) + ", " + str(value[2]) + ", " + str(value[3]) + ")"
        elif (datatype == 'VECTOR'):
            if (vector_size == 2):
                self.vector_size = vector_size
                self.result = "("
                for i in range(0, vector_size):
                    self.vector2[i] = value[i]
                    if (i == 0): #no comma in first iteration
                        self.result += str(value[i])
                    else:
                        self.result += ", " + str(value[i])
                self.result += ")"
            if (vector_size == 3):
                self.vector_size = vector_size
                self.result = "("
                for i in range(0, vector_size):
                    self.vector3[i] = value[i]
                    if (i == 0): #no comma in first iteration
                        self.result += str(value[i])
                    else:
                        self.result += ", " + str(value[i])
                self.result += ")"
            if (vector_size == 4):
                self.vector_size = vector_size
                self.result = "("
                for i in range(0, vector_size):
                    self.vector4[i] = value[i]
                    if (i == 0): #no comma in first iteration
                        self.result += str(value[i])
                    else:
                        self.result += ", " + str(value[i])
                self.result += ")"
        elif (datatype == 'DATA'):
            self.data = value
            self.result = str(value)
    
    # Optional function for drawing the socket input value
    def draw(self, context, layout, node, text):
        layout.label(text)

    # Socket color
    def draw_color(self, context, node):
        return (0.0, 0.4, 1.0, 1.0)

class DataSocket(NodeSocket):
    # Description string
    '''DataSocket'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'DataSocket'
    # Label for nice name display
    bl_label = 'Data'

    datatype = bpy.props.EnumProperty(name="Datatype",
                                    description="Just an example",
                                    items = datatype_list,
                                    default='OBJECTS')
    result = bpy.props.StringProperty()
    indent = bpy.props.IntProperty()
    
    def getValue(self):
        return self.result
    
    def setValue(self, result, indent):
        self.result = result
        self.indent = indent

    # Optional function for drawing the socket input value
    def draw(self, context, layout, node, text):
        layout.label(text)

    # Socket color
    def draw_color(self, context, node):
        return (1.0, 0.4, 0.216, 1.0)


# Mix-in class for all custom nodes in this tree type.
# Defines a poll function to enable instantiation.
class NoodleSoupTreeNode:
    @classmethod
    def poll(cls, ntree):
        return ntree.bl_idname == 'KhaliblooNoodleSoup'


class ObjectNode(Node, KhaliblooNoodleSoup):
    '''A custom node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'ObjectNode'
    # Label for nice name display
    bl_label = 'Object'
    # Icon identifier
    bl_icon = 'SOUND'

    # === Custom Properties ===
    template_all_objs = "for obj in bpy.data.objects:\n"
    indent_all_objs = 1
    
    template_filters = "{if (obj.type in ):\n" # ins filters at -4
    
    template_scene_objs = "for obj in bpy.context.scene.objects:\n"
    indent_scene_objs = 1
    
    template_selected_objs = "for obj in bpy.context.selected_objects:\n"
    indent_selected_objs = 1
    
    template_active_obj = "obj = bpy.context.active_object\n"
    indent_active_obj = 0
    
    template_specific_obj = "obj = bpy.data.objects[]\n" #ins name at -2
    indent_specific_obj = 0
    
    result = ""
    
    def processObjTypeFilters(self):
        list = []
        if (self.type_filters[0]):
            list.append('MESH')
        if (self.type_filters[1]):
            list.append('CURVE')
        if (self.type_filters[2]):
            list.append('ARMATURE')
        if (self.type_filters[3]):
            list.append('SURFACE')
        if (self.type_filters[4]):
            list.append('META')
        if (self.type_filters[5]):
            list.append('CAMERA')
        if (self.type_filters[6]):
            list.append('LAMP')
        if (self.type_filters[7]):
            list.append('SPEAKER')
        if (self.type_filters[8]):
            list.append('FONT')
        if (self.type_filters[9]):
            list.append('LATTICE')
        if (self.type_filters[10]):
            list.append('EMPTY')
        return list
        
        
    def update_obj_list_func(self):
        list = []
        for i in range(0, len(bpy.data.objects.keys())):
            list.append((bpy.data.objects.keys()[i], bpy.data.objects.keys()[i], ""))
        return list
    
    #obj_list = update_obj_list_func(Node)
    #FILTERS ORDER
    #filter_mesh = 0
    #filter_curve = 1
    #filter_armature = 2
    #filter_surface = 3
    #filter_meta = 4
    #filter_camera = 5
    #filter_lamp = 6
    #filter_speaker = 7
    #filter_font = 8
    #filter_lattice = 9
    #filter_empty = 10
    type_filters = bpy.props.BoolVectorProperty(name = "Filter",
                                                description = "Filter objects by type",
                                                size = 11,
                                                default = [True] * 11)
    use_filters = bpy.props.BoolProperty(name = "Filter",
                                        description = "Filter objects by type")
    use_iteration = bpy.props.BoolProperty(name = "Iterate",
                                        description = "Recalculate values for each iteration. Useful when random values are used.")
    obj_scope = bpy.props.EnumProperty(items=(
                                                ('ALL_OBJECTS', 'All Objects', ''), 
                                                ('SCENE_OBJECTS', 'Scene Objects', ''), 
                                                ('SELECTED_OBJECTS', 'Selected Objects', ''), 
                                                ('ACTIVE_OBJECT', 'Active Object', ''),
                                                ('SPECIFIC_OBJECT', 'Specific Object', '')),
                                                name='Scope',
                                                description='Scope of objects to operate on',
                                                default='SELECTED_OBJECTS')
    # obj_names = bpy.props.EnumProperty(items = obj_list,
                                        # name = "Identifier",
                                        # description = "Object name",
                                        # default = bpy.data.objects.keys()[0])
    obj_name = bpy.props.StringProperty(name = "Identifier",
                                        description = "Object name")

    # === Optional Functions ===
    def init(self, context):
        self.outputs.new('DataSocket', "Data")
        self.outputs[0].result = "result"

    def process(self):
        for input in self.inputs:
            for link in input.links:
                link.from_node.process()
        indent = 0
        if (self.obj_scope == 'ALL_OBJECTS'):
            result = self.template_all_objs
            indent = self.indent_all_objs
        elif (self.obj_scope == 'SCENE_OBJECTS'):
            result = self.template_scene_objs
            indent = self.indent_scene_objs
        elif (self.obj_scope == 'SELECTED_OBJECTS'):
            result = self.template_selected_objs
            indent = self.indent_selected_objs
        elif (self.obj_scope == 'ACTIVE_OBJECT'):
            result = self.template_active_obj
            indent = self.indent_active_obj
        elif (self.obj_scope == 'SPECIFIC_OBJECT'):
            result = insertString(self.obj_name, self.template_specific_obj, -2)
            indent = self.indent_specific_obj
        
        self.result = result
        self.outputs[0].setValue(self.result, indent)
        #Filters
        if (self.use_filters and self.obj_scope in ['ALL_OBJECTS', 'SCENE_OBJECTS', 'SELECTED_OBJECTS']):
            self.result += insertString(str(self.processObjTypeFilters()), self.template_filters, -4)
            self.outputs[0].setValue(self.result, indent + 1)
        
        
        
        #self.obj_list = self.update_obj_list_func()
        #self.obj_names.items = self.obj_list

    # Additional buttons displayed on the node.
    def draw_buttons(self, context, layout):
        layout.label("Data:")
        row = layout.row(align=True)
        row.prop(self, "obj_scope", "")
        if (self.obj_scope in ['ALL_OBJECTS', 'SCENE_OBJECTS', 'SELECTED_OBJECTS']):
            #layout.prop(self, "use_iteration")
            layout.prop(self, "use_filters")
            if (self.use_filters):
                row = layout.row()
                row.prop(self, "type_filters", "Meshes", index=0, toggle=True)
                row = layout.row()
                row.prop(self, "type_filters", "Curves", index=1, toggle=True)
                row.prop(self, "type_filters", "Armatures", index=2, toggle=True)
                row = layout.row()
                row.prop(self, "type_filters", "Surfaces", index=3, toggle=True)
                row.prop(self, "type_filters", "Metas", index=4, toggle=True)
                row = layout.row()
                row.prop(self, "type_filters", "Cameras", index=5, toggle=True)
                row.prop(self, "type_filters", "Lamps", index=6, toggle=True)
                row = layout.row()
                row.prop(self, "type_filters", "Speakers", index=7, toggle=True)
                row.prop(self, "type_filters", "Fonts", index=8, toggle=True)
                row = layout.row()
                row.prop(self, "type_filters", "Lattices", index=9, toggle=True)
                row.prop(self, "type_filters", "Empties", index=10, toggle=True)
        elif (self.obj_scope == 'SPECIFIC_OBJECT'):
            layout.prop(self, "obj_name", "", icon='OBJECT_DATAMODE')
        

    # Optional: custom label
    def draw_label(self):
        return "Object"

# class MaterialNode(Node, KhaliblooNoodleSoup):
    # '''A custom node'''
    # # Optional identifier string. If not explicitly defined, the python class name is used.
    # bl_idname = 'MaterialNode'
    # # Label for nice name display
    # bl_label = 'Material'
    # # Icon identifier
    # bl_icon = 'SOUND'

    # # === Custom Properties ===
    # template_all_mats = "for mat in bpy.data.materials:\n"
    # template_specific_mat = "mat = bpy.data.materials[""]\n" #ins at 26
    # template_all_obj_mats = ""
    # template_world_mats = ""
    # #template
    
    # def update_mat_list_func(self):
        # list = []
        # for i in range(0, len(bpy.data.materials.keys())):
            # list.append((bpy.data.materials.keys()[i], bpy.data.materials.keys()[i], ""))
        # return list
    
    # #mat_list = update_mat_list_func(Node)
    # #materials
    # mat_scope = bpy.props.EnumProperty(items=(
                                                # ('ALL_MATERIALS', 'All Materials', ''),
                                                # ('SPECIFIC_MATERIAL', 'Specific Material', '')),
                                                # name='Scope',
                                                # description='Scope of materials to operate on',
                                                # default='ALL_MATERIALS')
    # mat_scope_connected = bpy.props.EnumProperty(items=(
                                            # ('ALL_MATERIALS', 'All Materials', ''),
                                            # ('SELECTED_MATERIALS', 'Selected Materials', '')),
                                            # name='Scope',
                                            # description='Scope of materials to operate on',
                                            # default='ALL_MATERIALS')
    # # mat_names = bpy.props.EnumProperty(items = mat_list,
                                        # # name = "Identifier",
                                        # # description = "Material name",
                                        # # default = bpy.data.materials.keys()[0])
    # mat_name = bpy.props.StringProperty(name = "Identifier",
                                        # description = "Material name")

    # # === Optional Functions ===
    # def process(self):
        # for input in self.inputs:
            # for link in input.links:
                # link.from_node.process()
        # if (self.inputs[0].is_linked):
            # scope = self.mat_scope_connected
        # else:
            # scope = self.mat_scope
            # if (scope == 'ALL_MATERIALS'):
                # pass
            # elif (scope == 'SPECIFIC_MATERIAL'):
                # #result = 
                # pass
                
    # def init(self, context):
        # self.inputs.new('DataSocket', "Data")
        # for input in self.inputs:
            # input.link_limit = 1
        # self.outputs.new('DataSocket', "Data")

    # def update_socket(self):
        # #self.update()
        # print("weeeee")
        # #self.mat_list = self.update_mat_list_func()
        # #self.mat_names.items = self.mat_list

    # # Copy function to initialize a copied node from an existing one.
    # def copy(self, node):
        # print("Copying from node ", node)

    # # Free function to clean up on removal.
    # def free(self):
        # print("Removing node ", self, ", Goodbye!")

    # # Additional buttons displayed on the node.
    # def draw_buttons(self, context, layout):
        # layout.label("Data:")
        # if (self.inputs[0].is_linked):
            # layout.prop(self, "mat_scope_connected", "")
        # else:
            # layout.prop(self, "mat_scope", "")
            # if (self.mat_scope == 'SPECIFIC_MATERIAL'):
                # layout.prop(self, "mat_name", "", icon='MATERIAL')
        

    # # Optional: custom label
    # def draw_label(self):
        # return "Material"

class CompareNode(Node, KhaliblooNoodleSoup):
    '''A custom node'''
    bl_idname = 'CompareNode'
    bl_label = 'Compare'
    bl_icon = 'SOUND'

    # === Custom Properties ===
    template_equal_to = " == "
    template_not_equal_to = " != "
    template_greater_than = " > "
    template_less_than = " < "
    template_greater_equal = " >= "
    template_less_equal = " <= "
    template_contains = ".find() != -1" #ins A at 0, B at 6
    template_startswith = ".startswith()" #ins A at 0, B at 12
    template_endswith = ".endswith()" #ins A at 0, B at 10
    
    datatype = bpy.props.EnumProperty(items=(
                                ('NUMBER', 'Number', ''),
                                ('STRING', 'String', ''),
                                ('BOOL', 'Boolean', ''),
                                ('COLOR', 'Color', ''),
                                ('VECTOR', 'Vector', ''),
                                ('DATA', 'Data', '')
                                ),
                                name = "Type",
                                description = "Type of variables",
                                default = 'NUMBER')
    numberComparison = bpy.props.EnumProperty(items=(
                                ('EQUAL_TO', 'Equal to', ''),
                                ('NOT_EQUAL_TO', 'Not equal to', ''),
                                ('GREATER_THAN', 'Greater than', ''),
                                ('GREATER_THAN_OR_EQUAL_TO', 'Greater than or equal to', ''),
                                ('LESS_THAN', 'Less than', ''),
                                ('LESS_THAN_OR_EQUAL_TO', 'Less than or equal to', '')
                                ),
                                name = "Comparison",
                                description = "Type of comparison",
                                default = 'EQUAL_TO')
    stringComparison = bpy.props.EnumProperty(items=(
                                ('EQUAL_TO', 'Equal to', ''),
                                ('NOT_EQUAL_TO', 'Not equal to', ''),
                                ('CONTAINS', 'Contains', ''),
                                ('STARTS_WITH', 'Starts with', ''),
                                ('ENDS_WITH', 'Ends with', '')
                                ),
                                name = "Comparison",
                                description = "Type of comparison",
                                default = 'EQUAL_TO')
    boolComparison = bpy.props.EnumProperty(items=(
                                ('EQUAL_TO', 'Equal to', ''),
                                ('NOT_EQUAL_TO', 'Not equal to', '')
                                ),
                                name = "Comparison",
                                description = "Type of comparison",
                                default = 'EQUAL_TO')
    colorComparison = bpy.props.EnumProperty(items=(
                                ('EQUAL_TO', 'Equal to', ''),
                                ('NOT_EQUAL_TO', 'Not equal to', '')
                                ),
                                name = "Comparison",
                                description = "Type of comparison",
                                default = 'EQUAL_TO')
    vectorComparison = bpy.props.EnumProperty(items=(
                                ('EQUAL_TO', 'Equal to', ''),
                                ('NOT_EQUAL_TO', 'Not equal to', '')
                                ),
                                name = "Comparison",
                                description = "Type of comparison",
                                default = 'EQUAL_TO')
    dataComparison = bpy.props.EnumProperty(items=(
                                ('EQUAL_TO', 'Equal to', ''),
                                ('NOT_EQUAL_TO', 'Not equal to', '')
                                ),
                                name = "Comparison",
                                description = "Type of comparison",
                                default = 'EQUAL_TO')

    # === Optional Functions ===
    def process(self):
        #TODO: check if there are links, before wasting time on computations
        
        for input in self.inputs:
            for link in input.links:
                link.from_node.process()
        varA = self.inputs[0].links[0].from_socket
        varB = self.inputs[1].links[0].from_socket
        if (self.datatype == 'NUMBER'):
            comparison = self.numberComparison
        elif (self.datatype == 'STRING'):
            comparison = self.stringComparison
        elif (self.datatype == 'BOOL'):
            comparison = self.boolComparison
        elif (self.datatype == 'COLOR'):
            comparison = self.colorComparison
        elif (self.datatype == 'VECTOR'):
            comparison = self.vectorComparison
        else:
            comparison = self.dataComparison
        
        if (comparison == 'EQUAL_TO'):
            result = varA.result + self.template_equal_to + varB.result
        elif (comparison == 'NOT_EQUAL_TO'):
            result = varA.result + self.template_not_equal_to + varB.result
        elif (comparison == 'GREATER_THAN'):
            result = varA.result + self.template_greater_than + varB.result
        elif (comparison == 'LESS_THAN'):
            result = varA.result + self.template_less_than + varB.result
        elif (comparison == 'GREATER_THAN_OR_EQUAL_TO'):
            result = varA.result + self.template_greater_equal + varB.result
        elif (comparison == 'LESS_THAN_OR_EQUAL_TO'):
            result = varA.result + self.template_less_equal + varB.result
        elif (comparison == 'CONTAINS'):
            result = varA.result + insertString(varB.result, self.template_contains, 6)
        elif (comparison == 'STARTS_WITH'):
            result = varA.result + insertString(varB.result, self.template_startswith, 12)
        elif (comparison == 'ENDS_WITH'):
            result = varA.result + insertString(varB.result, self.template_endswith, 10)
            
        self.outputs[0].setValue(result, datatype = 'DATA')
        
    def init(self, context):
        self.inputs.new('VariableSocket', "A") #0
        self.inputs.new('VariableSocket', "B") #1
        for input in self.inputs:
            input.link_limit = 1

        self.outputs.new('VariableSocket', "Result")

    # Additional buttons displayed on the node.
    def draw_buttons(self, context, layout):
        layout.prop(self, "datatype")
        if (self.datatype == 'NUMBER'):
            layout.prop(self, "numberComparison", "")
        elif (self.datatype == 'STRING'):
            layout.prop(self, "stringComparison", "")
        elif (self.datatype == 'BOOL'):
            layout.prop(self, "boolComparison", "")
        elif (self.datatype == 'COLOR'):
            layout.prop(self, "colorComparison", "")
        elif (self.datatype == 'VECTOR'):
            layout.prop(self, "vectorComparison", "")
        elif (self.datatype == 'DATA'):
            layout.prop(self, "dataComparison", "")
        
        

    # Detail buttons in the sidebar.
    # If this function is not defined, the draw_buttons function is used instead
    #def draw_buttons_ext(self, context, layout):
        #layout.prop(self, "myFloatProperty")
        # myStringProperty button will only be visible in the sidebar
        #layout.prop(self, "myStringProperty")

    # Optional: custom label
    # Explicit user label overrides this, but here we can define a label dynamically
    def draw_label(self):
        return "Compare"

class IfElseNode(Node, KhaliblooNoodleSoup):
    '''If condition is true, the result is the first variable... Else, the result is the second variable'''
    bl_idname = 'IfElseNode'
    bl_label = 'If...Else'
    bl_icon = 'SOUND'

    # === Custom Properties ===
    template_if = "if (): \n{" #ins condition at 4
    template_elif = "elif (): \n{" #ins condition at 6
    template_assign = "value = " #ins value1 at -1
    template_else = "else: \n{" #ins value2 at -2

    # === Optional Functions ===
    def process(self):
        for input in self.inputs:
            for link in input.links:
                link.from_node.process()
        condition = self.inputs[0].links[0].from_socket
        value1 = self.inputs[1].links[0].from_socket
        if (value1.result.startswith("if ")):
            result = insertString(condition.result, self.template_if[:-1], 4)
            result += indentLines(value1.result, 1) + template_linebreak
        else:
            result = insertString(condition.result, self.template_if, 4)
            result += self.template_assign + value1.result + template_linebreak
        #else
        if (self.inputs[2].is_linked):
            value2 = self.inputs[2].links[0].from_socket
            if (value2.result.startswith("if ")):
                result += self.template_else[:-1] + indentLines(value2.result, 1) + template_linebreak
            else:
                result += self.template_else + self.template_assign + value2.result + template_linebreak
        self.outputs[0].result = result
        
    def init(self, context):
        self.inputs.new('VariableSocket', "Condition")
        self.inputs.new('VariableSocket', "Variable A")
        self.inputs.new('VariableSocket', "Variable B")
        for input in self.inputs:
            input.link_limit = 1

        self.outputs.new('VariableSocket', "Result")

    # Additional buttons displayed on the node.
    def draw_buttons(self, context, layout):
        pass
        

    # Optional: custom label
    # Explicit user label overrides this, but here we can define a label dynamically
    def draw_label(self):
        return "If...Else"


class VariableNode(Node, KhaliblooNoodleSoup):
    # === Basics ===
    # Description string
    '''A custom node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'VariableNode'
    # Label for nice name display
    bl_label = 'Variable'
    # Icon identifier
    bl_icon = 'SOUND'

    # === Custom Properties ===
    # These work just like custom properties in ID data blocks
    
    def update_variable_type_func(self,context):
        if (self.variableType == 'INT'):
            self.outputs[0].enabled = True
            self.outputs[1].enabled = False
            self.outputs[2].enabled = False
            self.outputs[3].enabled = False
            self.outputs[4].enabled = False
            self.outputs[5].enabled = False
        if (self.variableType == 'FLOAT'):
            self.outputs[0].enabled = False
            self.outputs[1].enabled = True
            self.outputs[2].enabled = False
            self.outputs[3].enabled = False
            self.outputs[4].enabled = False
            self.outputs[5].enabled = False
        elif (self.variableType == 'STRING'):
            self.outputs[0].enabled = False
            self.outputs[1].enabled = False
            self.outputs[2].enabled = True
            self.outputs[3].enabled = False
            self.outputs[4].enabled = False
            self.outputs[5].enabled = False
        elif (self.variableType == 'BOOL'):
            self.outputs[0].enabled = False
            self.outputs[1].enabled = False
            self.outputs[2].enabled = False
            self.outputs[3].enabled = True
            self.outputs[4].enabled = False
            self.outputs[5].enabled = False
        elif (self.variableType == 'COLOR'):
            self.outputs[0].enabled = False
            self.outputs[1].enabled = False
            self.outputs[2].enabled = False
            self.outputs[3].enabled = False
            self.outputs[4].enabled = True
            self.outputs[5].enabled = False
        elif (self.variableType == 'VECTOR'):
            self.outputs[0].enabled = False
            self.outputs[1].enabled = False
            self.outputs[2].enabled = False
            self.outputs[3].enabled = False
            self.outputs[4].enabled = False
            self.outputs[5].enabled = True
    
    def update_bool_func(self, context):
        if (self.boolMenu == 'TRUE'):
            self.bool = True
        else:
            self.bool = False
            
    int = bpy.props.IntProperty(name = "Int",
                                description = "Integer number")
    float = bpy.props.FloatProperty(name = "Float",
                                    description = "Floating point number")
    string = bpy.props.StringProperty(name = "String",
                                    description = "String")
    bool = bpy.props.BoolProperty(name = "Boolean",
                                description = "Boolean value",
                                default = True)
    boolMenu = bpy.props.EnumProperty(items=(
                                ('TRUE', 'True', ''),
                                ('FALSE', 'False', '')
                                ),
                                name = "Boolean",
                                description = "Boolean value",
                                default = 'TRUE',
                                update = update_bool_func
                                )
    color = bpy.props.FloatVectorProperty(name = "Color",
                                        description = "Color value",
                                        default = (1.0, 1.0, 1.0, 1.0),
                                        min = 0.0,
                                        max = 1.0,
                                        subtype = 'COLOR',
                                        size = 4)
    #vector2 = bpy.props.FloatVectorProperty(name = "Vector",
    #                                    description = "Vector value",
    #                                    size = 2
    #                                    )
    #vector3 = bpy.props.FloatVectorProperty(name = "Vector",
    #                                    description = "Vector value",
    #                                    size = 3
    #                                    )
    #vector4 = bpy.props.FloatVectorProperty(name = "Vector",
    #                                    description = "Vector value",
    #                                    size = 4
    #                                    )
    vector = bpy.props.FloatVectorProperty(name = "Vector",
                                        description = "Vector value",
                                        size = 4
                                        )
    vector_size = bpy.props.IntProperty(name = "Vector Size",
                                        description = "Number of elements in the vector",
                                        default = 3,
                                        min = 2,
                                        max = 4
                                        )
    
    datatype = bpy.props.EnumProperty(items=(
                                ('INT', 'Integer', ''),
                                ('FLOAT', 'Float', ''),
                                ('STRING', 'String', ''),
                                ('BOOL', 'Boolean', ''),
                                ('COLOR', 'Color', ''),
                                ('VECTOR', 'Vector', '')
                                ),
                                name = "Type",
                                description = "Type of variable",
                                default = 'INT',
                                update = update_variable_type_func)
    
    use_random = bpy.props.BoolProperty(name = "Random",
                                description = "Generate different random value each iteration")
    min_random_int = bpy.props.IntProperty(name = "Min",
                                description = "Minimum random integer number (Inclusive)",
                                default = 0)
    min_random_float = bpy.props.FloatProperty(name = "Min",
                                    description = "Minimum random floating point number (Inclusive)",
                                    default = 0.0)
    max_random_int = bpy.props.IntProperty(name = "Max",
                                description = "Minimum random integer number (Inclusive)",
                                default = 10)
    max_random_float = bpy.props.FloatProperty(name = "Max",
                                    description = "Maximum random floating point number (Inclusive)",
                                    default = 10.0)

    # === Optional Functions ===
    def process(self):
        for input in self.inputs:
            for link in input.links:
                link.from_node.process()
                input = link.from_socket
        
        for output in self.outputs:
            if (output.enabled and output.is_linked):
                global use_random
                if (use_random == False):
                    use_random = self.use_random
                if (self.datatype == 'INT'):
                    output.setValue(self.int, self.datatype, 
                                     use_random = self.use_random,
                                     min_random_int = self.min_random_int, 
                                     max_random_int = self.max_random_int)
                elif (self.datatype == 'FLOAT'):
                    output.setValue(self.float, self.datatype,
                                     use_random = self.use_random,
                                     min_random_int = int(self.min_random_float), 
                                     max_random_int = int(self.max_random_float))
                elif (self.datatype == 'STRING'):
                    output.setValue(self.string, self.datatype)
                elif (self.datatype == 'BOOL'):
                    output.setValue(self.bool, self.datatype, 
                                    use_random = self.use_random)
                elif (self.datatype == 'COLOR'):
                    output.setValue(self.color, self.datatype)
                elif (self.datatype == 'VECTOR'):
                    output.setValue(self.vector, self.datatype, vector_size = self.vector_size)
                
            
    
    def init(self, context):
        self.outputs.new('VariableSocket', "Value")
#        self.outputs.new('NodeSocketInt', "Int") #0
#        self.outputs.new('NodeSocketFloat', "Float") #1
#        self.outputs[1].enabled = False
#        self.outputs.new('NodeSocketString', "String") #2
#        self.outputs[2].enabled = False
#        self.outputs.new('NodeSocketBool', "Bool") #3
#        self.outputs[3].enabled = False
#        self.outputs.new('NodeSocketColor', "Color") #4
#        self.outputs[4].enabled = False
#        self.outputs.new('NodeSocketVector', "Vector") #5
#        self.outputs[5].enabled = False

    # Additional buttons displayed on the node.
    def draw_buttons(self, context, layout):
        layout.prop(self, "datatype")
        if (self.datatype == 'INT'):
            layout.separator()
            layout.prop(self, "use_random")
            if (self.use_random):
                col = layout.column(align = True)
                col.prop(self, "min_random_int")
                col.prop(self, "max_random_int")
            else:
                layout.prop(self, "int")
        elif (self.datatype == 'FLOAT'):
            layout.separator()
            layout.prop(self, "use_random")
            if (self.use_random):
                col = layout.column(align = True)
                col.prop(self, "min_random_float")
                col.prop(self, "max_random_float")
            else:
                layout.prop(self, "float")
        elif (self.datatype == 'STRING'):
            layout.separator()
            layout.prop(self, "string", "")
        elif (self.datatype == 'BOOL'):
            layout.separator()
            layout.prop(self, "use_random")
            if (self.use_random == False):
                layout.prop(self, "boolMenu", "")
        elif (self.datatype == 'COLOR'):
            layout.separator()
            layout.separator()
            layout.template_color_picker(self, "color", value_slider=True)
            layout.prop(self, "color", "")
        elif (self.datatype == 'VECTOR'):
            layout.prop(self, "vector_size")
            layout.separator()
            if (self.vector_size == 2):
                col = layout.column(align=True)
                for i in range (0, self.vector_size):
                    col.prop(self, "vector", "Item " + str(i+1), index = i)
            elif (self.vector_size == 3):
                col = layout.column(align=True)
                for i in range (0, self.vector_size):
                    col.prop(self, "vector", "Item " + str(i+1), index = i)
            elif (self.vector_size == 4):
                col = layout.column(align=True)
                for i in range (0, self.vector_size):
                    col.prop(self, "vector", "Item " + str(i+1), index = i)
        

    # Optional: custom label
    def draw_label(self):
        return "Variable"


class LogicOperatorNode(Node, KhaliblooNoodleSoup):
    # === Basics ===
    # Description string
    '''A custom node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'LogicOperatorNode'
    # Label for nice name display
    bl_label = 'Logic Operator'
    # Icon identifier
    bl_icon = 'SOUND'

    # === Custom Properties ===
    template_and = "( and )" #ins at 1 and 6
    template_or = "( or )" #ins at 1 and 5
    template_not = "(not )" #ins at 5
    
    def update_logic_func(self, context):
        if (self.logic == 'NOT'):
            self.inputs[1].enabled = False
        else:
            self.inputs[1].enabled = True
    logic = bpy.props.EnumProperty(items=(
                                ('AND', 'And', ''),
                                ('OR', 'Or', ''),
                                ('NOT', 'Not', '')
                                ),
                                name = "Logic",
                                description = "Type of logic",
                                default = 'AND',
                                update = update_logic_func)

    # === Optional Functions ===
    def process(self):
        for input in self.inputs:
            for link in input.links:
                link.from_node.process()
                input = link.from_socket
        if (self.logic == 'AND'):
            varA = self.inputs[0].links[0].from_socket
            varB = self.inputs[1].links[0].from_socket
            result = insertStrings([varA.result, varB.result], self.template_and, [1, 6])
        elif (self.logic == 'OR'):
            varA = self.inputs[0].links[0].from_socket
            varB = self.inputs[1].links[0].from_socket
            result = insertStrings([varA.result, varB.result], self.template_or, [1, 5])
        elif (self.logic == 'NOT'):
            varA = self.inputs[0].links[0].from_socket
            result = insertString(varA.result, self.template_not, 5)
        
        self.outputs[0].setValue(result, datatype = 'DATA')
                
    def init(self, context):
        self.inputs.new('VariableSocket', "Condition A")
        self.inputs.new('VariableSocket', "Condition B")
        for input in self.inputs:
            input.link_limit = 1
        
        self.outputs.new('VariableSocket', "Result")

    # Additional buttons displayed on the node.
    def draw_buttons(self, context, layout):
        layout.prop(self, "logic")
        

    # Optional: custom label
    def draw_label(self):
        return "Logic Operator"

# class GetPropertyNode(Node, KhaliblooNoodleSoup):
    # # === Basics ===
    # # Description string
    # '''A custom node'''
    # # Optional identifier string. If not explicitly defined, the python class name is used.
    # bl_idname = 'GetPropertyNode'
    # # Label for nice name display
    # bl_label = 'Get Property'
    # # Icon identifier
    # bl_icon = 'SOUND'

    # # === Custom Properties ===
    # prop = bpy.props.StringProperty(name = "Prop",
                                    # description = "Identifier of property",
                                    # default = "name")

    # # === Optional Functions ===
    # def process(self):
        # for input in self.inputs:
            # for link in input.links:
                # link.from_node.process()
                # input = link.from_socket
                
    # def init(self, context):
        # self.inputs.new('DataSocket', "Data")
        # for input in self.inputs:
            # input.link_limit = 1
        
        # self.outputs.new('VariableSocket', "Value")

    # # Additional buttons displayed on the node.
    # def draw_buttons(self, context, layout):
        # layout.label("Property Identifier:")
        # layout.prop(self, "prop", "")
        

    # # Optional: custom label
    # def draw_label(self):
        # return "Get Property"

class SetPropertyNode(Node, KhaliblooNoodleSoup):
    '''A custom node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'SetPropertyNode'
    # Label for nice name display
    bl_label = 'Set Property'
    # Icon identifier
    bl_icon = 'SOUND'

    # === Custom Properties ===
    template_setprop_01 = "if (value):\n{"
    template_setprop_02 = "obj. = value" #ins prop at 4
    template_setvalue = "value = \n" #ins value at -1
    index = bpy.props.IntProperty(name = "Index",
                                description = "Actions with the lowest indices will be performed first",
                                default = 0)
    mode = bpy.props.EnumProperty(items=(
                                    ('OBJECT', 'Object Mode', ''), 
                                    ('EDIT', 'Edit Mode', ''), 
                                    ('POSE', 'Pose Mode', ''), 
                                    ('SCULPT', 'Sculpt Mode', ''), 
                                    ('VERTEX_PAINT', 'Vertex Paint Mode', ''), 
                                    ('WEIGHT_PAINT', 'Weight Paint Mode', ''), 
                                    ('TEXTURE_PAINT', 'Texture Paint Mode', ''), 
                                    ('PARTICLE_EDIT', 'Particle Edit Mode', '')), 
                                    name = "Mode", 
                                    description = "Mode in which to perform action", 
                                    default='OBJECT')
    prop = bpy.props.StringProperty(name = "Prop",
                                    description = "Identifier of property",
                                    default = "name")
    use_per_iteration = bpy.props.BoolProperty(name = "Per Iteration",
                                                description = "Recalculate values for each iteration. Useful when a different random value is needed for each iteration")
    generated_script = ""

    # === Optional Functions ===
    def process(self):
        global use_random
        use_random = False
        for input in self.inputs:
            for link in input.links:
                link.from_node.process()
                input = link.from_socket
        import_statements = template_import_bpy
        if (use_random):
            import_statements += template_import_random
        data = self.inputs[0].links[0].from_socket.result
        indent = self.inputs[0].links[0].from_socket.indent
        value = self.inputs[1].links[0].from_socket.result
        
        #if nodes do not need assignment statements
        if (value.startswith("if ")):
            value_calculations = value
        else:
            value_calculations = insertString(value, self.template_setvalue, -1)
        #should value calculations happen for each iteration?
        if (self.use_per_iteration):
            result = import_statements + template_linebreak + template_value_init + data
            result += indentLines(value_calculations, indent)
        else:
            result = import_statements + template_linebreak + template_value_init + value_calculations + data
        assignment_statement = (template_indent * indent) + self.template_setprop_01
        assignment_statement += (template_indent * indent) + insertString(self.prop, self.template_setprop_02, 4)
        result += assignment_statement
        self.generated_script = result
        
    def init(self, context):
        self.inputs.new('DataSocket', "Data")
        self.inputs.new('VariableSocket', "Value")
        for input in self.inputs:
            input.link_limit = 1

    # Additional buttons displayed on the node.
    def draw_buttons(self, context, layout):
        layout.label(self.inputs[0].getValue())
        layout.prop(self, "index")
        
        layout.prop(self, "mode", "")
        layout.separator()
        layout.label("Property Identifier:")
        layout.prop(self, "prop", "")
        layout.prop(self, "use_per_iteration")
        layout.separator()
        

    # Optional: custom label
    def draw_label(self):
        return "Set Property"


# class ActionNode(Node, KhaliblooNoodleSoup):
    # # === Basics ===
    # # Description string
    # '''A custom node'''
    # # Optional identifier string. If not explicitly defined, the python class name is used.
    # bl_idname = 'ActionNode'
    # # Label for nice name display
    # bl_label = 'Action'
    # # Icon identifier
    # bl_icon = 'SOUND'

    # # === Custom Properties ===
    # index = bpy.props.IntProperty(name = "Index",
                                # description = "Actions with the lowest indices will be performed first",
                                # default = 0)
    # actionClass = bpy.props.EnumProperty(items=(
                                # ('ARMATURE', 'Armature', ''),
                                # ('CAMERA', 'Camera', ''),
                                # ('MESH', 'Mesh', ''),
                                # ('OBJECT', 'Object', ''),
                                # ('SCENE', 'Scene', '')
                                # ),
                                # name = "Action Class",
                                # description = "Class of action",
                                # default = 'OBJECT')
    # mode = bpy.props.EnumProperty(items=(
                                    # ('OBJECT', 'Object Mode', ''), 
                                    # ('EDIT', 'Edit Mode', ''), 
                                    # ('POSE', 'Pose Mode', ''), 
                                    # ('SCULPT', 'Sculpt Mode', ''), 
                                    # ('VERTEX_PAINT', 'Vertex Paint Mode', ''), 
                                    # ('WEIGHT_PAINT', 'Weight Paint Mode', ''), 
                                    # ('TEXTURE_PAINT', 'Texture Paint Mode', ''), 
                                    # ('PARTICLE_EDIT', 'Particle Edit Mode', '')), 
                                    # name = "Mode", 
                                    # description = "Mode in which to perform action", 
                                    # default='OBJECT')

    # # === Optional Functions ===
    # def init(self, context):
        # self.inputs.new('DataSocket', "Data")
        # for input in self.inputs:
            # input.link_limit = 1

    # # Additional buttons displayed on the node.
    # def draw_buttons(self, context, layout):
        # layout.prop(self, "index")
        # layout.separator()
        # layout.prop(self, "mode", "")
        # layout.prop(self, "actionClass", "")
        # #if actionClass == so and so,
            # #layout.prop(self, "objectActions")
        

    # # Optional: custom label
    # def draw_label(self):
        # return "Action"


# class ExecuteNode(Node, KhaliblooNoodleSoup):
    # '''Execute nodetree'''
    # bl_idname = 'ExecuteNode'
    # bl_label = 'Execute'
    # bl_icon = 'SOUND'

    # # === Custom Properties ===
    # def update_slots_func(self, context):
        # currentSlots = len(self.inputs)
        # difference = self.slots - currentSlots
        # if (difference > 0):
            # #we need more slots
            # for i in range(0, difference):
                # #TODO: replace with actionsocket
                # self.inputs.new('VariableSocket', 'Action ' + str(currentSlots + 1))
                # currentSlots += 1
        # elif (difference < 0):
            # #we need to remove slots
            # currentSlots = self.slots
            # for i in range(0, -difference):
                # self.inputs.remove(self.inputs[-1])
        
    # #slots is the number of slots requested by user
    # slots = bpy.props.IntProperty(name = "Slots",
                                    # description = "Number of slots",
                                    # default = 1,
                                    # min = 1,
                                    # update = update_slots_func)


    # # === Optional Functions ===
    # def init(self, context):
        # #TODO: replace with actionsocket
        # self.inputs.new('VariableSocket', "Action 1")
        # for input in self.inputs:
            # input.link_limit = 1

    # # Additional buttons displayed on the node.
    # def draw_buttons(self, context, layout):
        # layout.prop(self, "slots")
        

    # # Optional: custom label
    # def draw_label(self):
        # return "Execute"


# class CombineVectorNode(Node, KhaliblooNoodleSoup):
    # '''Combine Vector'''
    # bl_idname = 'CombineVectorNode'
    # bl_label = 'Combine Vector'
    # bl_icon = 'SOUND'

    # # === Custom Properties ===
    # def update_slots_func(self, context):
        # currentSlots = len(self.inputs)
        # difference = self.slots - currentSlots
        # if (difference > 0):
            # #we need more slots
            # for i in range(0, difference):
                # self.inputs.new('NodeSocketFloat', 'Item ' + str(currentSlots + 1))
                # currentSlots += 1
        # elif (difference < 0):
            # #we need to remove slots
            # currentSlots = self.slots
            # for i in range(0, -difference):
                # self.inputs.remove(self.inputs[-1])
        
    # #slots is the number of slots requested by user
    # slots = bpy.props.IntProperty(name = "Slots",
                                    # description = "Number of slots",
                                    # default = 3,
                                    # min = 2,
                                    # update = update_slots_func)


    # # === Optional Functions ===
    # def init(self, context):
        # self.inputs.new('NodeSocketFloat', "Item 1")
        # self.inputs.new('NodeSocketFloat', "Item 2")
        # self.inputs.new('NodeSocketFloat', "Item 3")
        # for input in self.inputs:
            # input.link_limit = 1
        
        # self.outputs.new('NodeSocketVector', "Element 1")

    # # Additional buttons displayed on the node.
    # def draw_buttons(self, context, layout):
        # layout.prop(self, "slots")
        # layout.separator()
        

    # # Optional: custom label
    # def draw_label(self):
        # return "Combine Vector"


# class SeparateVectorNode(Node, KhaliblooNoodleSoup):
    # '''Separate Vector'''
    # bl_idname = 'SeparateVectorNode'
    # bl_label = 'Separate Vector'
    # bl_icon = 'SOUND'

    # # === Custom Properties ===
    # def update_slots_func(self, context):
        # currentSlots = len(self.outputs)
        # difference = self.slots - currentSlots
        # if (difference > 0):
            # #we need more slots
            # for i in range(0, difference):
                # self.outputs.new('NodeSocketFloat', 'Item ' + str(currentSlots + 1))
                # currentSlots += 1
        # elif (difference < 0):
            # #we need to remove slots
            # currentSlots = self.slots
            # for i in range(0, -difference):
                # self.outputs.remove(self.outputs[-1])
        
    # #slots is the number of slots requested by user
    # slots = bpy.props.IntProperty(name = "Slots",
                                    # description = "Number of slots",
                                    # default = 3,
                                    # min = 2,
                                    # update = update_slots_func)


    # # === Optional Functions ===
    # def init(self, context):
        # self.inputs.new('NodeSocketVector', "Vector")
        # for input in self.inputs:
            # input.link_limit = 1
        
        # self.outputs.new('NodeSocketFloat', "Item 1")
        # self.outputs.new('NodeSocketFloat', "Item 2")
        # self.outputs.new('NodeSocketFloat', "Item 3")

    # # Additional buttons displayed on the node.
    # def draw_buttons(self, context, layout):
        # layout.separator()
        # layout.prop(self, "slots")
        # layout.separator()
        

    # # Optional: custom label
    # def draw_label(self):
        # return "Separate Vector"





### Node Categories ###
# Node categories are a python system for automatically
# extending the Add menu, toolbar panels and search operator.
# For more examples see release/scripts/startup/nodeitems_builtins.py

import nodeitems_utils
from nodeitems_utils import NodeCategory, NodeItem

# our own base class with an appropriate poll function,
# so the categories only show up in our own tree type
class MyNodeCategory(NodeCategory):
    @classmethod
    def poll(cls, context):
        return context.space_data.tree_type == 'KhaliblooNoodleSoup'
 
# all categories in a list
node_categories = [
    # identifier, label, items list
    MyNodeCategory("Input", "Input", items=[
        NodeItem("ObjectNode"),
        #NodeItem("MaterialNode"),
        NodeItem("VariableNode"),
        #NodeItem("GetPropertyNode")
        ]),
    MyNodeCategory("Action", "Action", items=[
        NodeItem("SetPropertyNode"),
        #NodeItem("ActionNode"),
        #NodeItem("ExecuteNode")
        ]),
    #MyNodeCategory("Math", "Math", items=[
        #NodeItem("CombineVectorNode"),
        #NodeItem("SeparateVectorNode")
        #]),
    MyNodeCategory("Logic", "Logic", items=[
        NodeItem("IfElseNode"),
        NodeItem("CompareNode"),
        NodeItem("LogicOperatorNode")
        ])
]


# def register():
    # bpy.utils.register_module(__name__)
    # #nodeitems_utils.unregister_node_categories("KHALIBLOO NOODLE SOUP")
    # nodeitems_utils.register_node_categories("KHALIBLOO NOODLE SOUP", node_categories)
    # print("nodes registered")


# def unregister():
    # nodeitems_utils.unregister_node_categories("KHALIBLOO NOODLE SOUP")
    # bpy.utils.unregister_module(__name__)


# if __name__ == "__main__":
    # register()
