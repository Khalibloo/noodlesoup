# <pep8 compliant>
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Khalibloo Noodle Soup",
    "version": (0, 1),
    "author": "Khalifa Lame",
    "blender": (2, 76, 0),
    "description": "Visual Scripting in the node editor.",
    "location": "Node editor",
    "category": "Khalibloo"}

import bpy
from . import nodes
from . import ops
import nodeitems_utils

#============================================================================
# DRAW PANEL
#============================================================================
class KhaliblooNoodleSoupPanel(bpy.types.Panel):
    """Khalibloo Noodle Soup Panel"""
    bl_label = "Khalibloo Panel"
    bl_idname = "KhaliblooNoodleSoupPanel"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'

    @classmethod
    def poll(cls, context):
        space = context.space_data
        return space.type == 'NODE_EDITOR' and space.tree_type == 'KhaliblooNoodleSoup' and space.edit_tree is not None

    def draw(self, context):
        layout = self.layout

        layout.operator("node.khalibloo_noodle_soup_execute")


def register():
    #nodes.initialize()
    nodeitems_utils.register_node_categories("KHALIBLOO NOODLE SOUP", nodes.node_categories)
    bpy.utils.register_module(__name__)


def unregister():
    bpy.utils.unregister_module(__name__)
    nodeitems_utils.unregister_node_categories("KHALIBLOO NOODLE SOUP")

if __name__ == "__main__":
    register()
