import bpy
import os

def decode(text):
    linebreak = "\n"
    tab = "    "
    indent = "{"
    data = text.splitlines()
    result = []
    for i in range(0, len(data)):
        line = data[i]
        indentCount = 0
        while (line.startswith(indent)):
            indentCount += 1
            line = line[1:]
        #add indentation and line break
        line = (tab * indentCount) + line
        line = line + linebreak
        result.append(line)
    return "".join(result)

def runScript(text):
    filepath = os.path.join(working_dir, "temp.py")
    try:
        os.mkdir(working_dir)
    except(FileExistsError):
        pass
    try:
        os.remove(filepath)
    except(FileNotFoundError):
        pass
    file = open(filepath, mode='x')
    file.write(text)
    file.close()
    bpy.ops.script.python_file_run(filepath=filepath)

def main(operator, context):
    space = context.space_data
    node_tree = space.node_tree
    node_active = context.active_node
    node_selected = context.selected_nodes
    nodes = node_tree.nodes
    setprop_nodes = []

    # now we have the context, perform a simple operation
#    for node in nodes:
#        if (node.bl_idname == 'ObjectNode'):
#            if (not node.mute):
#                node.process()
    for node in nodes:
        if (node.bl_idname == 'SetPropertyNode'):
            setprop_nodes.append(node)

    for node in setprop_nodes:
        if (node.inputs[0].is_linked and node.inputs[1].is_linked):
            node.process()
            for link in node.inputs[0].links:
                result = node.generated_script
                #create text block for debugging
                #text = bpy.data.texts.new("noodle_soup_bowl" + str(node.index))
                text = bpy.data.texts["noodle_soup_bowl0"]
                text.from_string(decode(result))
                #run
                #runScript(result)

class KhaliblooNoodleSoupExecute(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "node.khalibloo_noodle_soup_execute"
    bl_label = "Execute"

    @classmethod
    def poll(cls, context):
        space = context.space_data
        return space.type == 'NODE_EDITOR' and space.tree_type == 'KhaliblooNoodleSoup'

    def execute(self, context):
        main(self, context)
        return {'FINISHED'}


def register():
    bpy.utils.register_class(KhaliblooNoodleSoupExecute)


def unregister():
    bpy.utils.unregister_class(KhaliblooNoodleSoupExecute)


if __name__ == "__main__":
    register()
